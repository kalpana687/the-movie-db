import {
  Component,
  OnInit
} from '@angular/core';
import {
  DomSanitizer
} from '@angular/platform-browser';
import {
  AppService
} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'movie';
  movies: Array < any > = [];
  loader: boolean = true;

  searchData = {
    page: 1,
    sort_by: 'popularity.desc'
  }

  constructor(private appService: AppService, public sanitizer: DomSanitizer) {}
  ngOnInit(): void {
    this.getMovies(this.searchData);
  }

  getMovies(searchData) {
    this.appService.getImages(searchData).subscribe((res: any) => {
      this.movies = res.results;
      this.loader = false;
    }, error => {
      this.loader = false;
    })
  }

  pageChanged(event) {
    this.searchData.page = event;
    this.getMovies(this.searchData);
  }

  sortOptions(value) {
    this.searchData.sort_by = value;
    this.getMovies(this.searchData);
  }
}

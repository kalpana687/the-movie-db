import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  getImages(searchData) {   
    let params = new HttpParams();
    params = params.append('api_key', environment.apiKey);
    params = params.append('page' , searchData.page);
    params = params.append('sort_by' , searchData.sort_by);    
    return this.http.get(`${environment.apiUrl}discover/movie`, {params: params});
  }
}
